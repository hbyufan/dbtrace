package john.walker.log;

import org.apache.log4j.Logger;

public class Log4jLogger extends AbstractLogger {

	private static Logger logger = Logger.getLogger(Log4jLogger.class);

	@Override
	public void debug(String msg) {
		logger.debug(msg);
	}


	@Override
	public void info(String msg) {
		logger.info(msg);
	}


	@Override
	public void warn(String msg) {
		logger.warn(msg);
	}


	@Override
	public void error(String msg) {
		logger.error(msg);
	}


	@Override
	public void fatal(String msg) {
		logger.fatal(msg);
	}


	@Override
	public void newLine() {
		logger.info("");
	}

}
